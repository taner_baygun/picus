# Welcome everyone this project is developed by Taner Baygün at 21.06.2021

## To Run

Go to project directory then run npm install then npm start

## How to Use

### Filters

For Age and Diet dropdown menu for Taxonomy text field can be used

Reset button switches all filters to default value

* Note: for sample version 3 different filters are implemented (Age, Diet, Taxonomy)

### Graph

X axis is dates, Y axis total number of creature status

Slider under the graph can be used to navitage through X axis

With slider above the graph, sample rate for the graph (number of items appeared on the graph) can be selected.

### General

For survival percentage last record is considered as it is the most recent. While calculating unknown cases are neglected