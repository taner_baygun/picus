class Creature {
    constructor(props){        
        this.status = props.status;
        this.age = props.age;
        this.diet = props.diet;
        this.taxonomy = props.taxonomy;
        this.id = props.id;
    }
    
}
export default Creature;