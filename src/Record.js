import Creature from "./Creature";

class Record {
    constructor(props){
        this.date = props.date;
        this.creatureList = props.creatureList.map(index => {
            return new Creature({
                status: index.status,
                age: index.age,
                diet: index.diet,
                taxonomy: index.taxonomy,
                id: index.id
            });
        }
        );
    }
}

export default Record;