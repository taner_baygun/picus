import Record from './Record';
import { recordList } from './Constants.js';

export function parseRecord(record) {
  let date = normalizeDate(record[0]);
  let creatureList = record[1];

  return new Record({date, creatureList});
}

function normalizeDate(rawDate){
  rawDate = rawDate.replace('T', ' ');
  rawDate = rawDate.replace(new RegExp("\\..*"), '')
  
  return new Date(rawDate)
}

export function deadOrAliveCount(item){
  let temp = [[],[],[]];
  
  item.filter(index => {
    if(index.status === "alive"){
      temp[1].push(index)
    } else if (index.status === "dead"){
      temp[0].push(index)
    } else {
      temp[2].push(index)
    }
    return index.status;
  })
  
  return temp;
}

export const filterList = (tmpList, filter) => {
    
  recordList.map(record => {
    let tmpRec = {...record}
    tmpRec.creatureList = [];
    
    record.creatureList.filter(creature => {
      let taxonomy;
      creature.taxonomy.map(item => taxonomy += item);
      if((filter.age.length === 0 || creature.age === filter.age)
        && (filter.diet.length === 0 || creature.diet === filter.diet)
        && (filter.taxonomy.length < 3 || taxonomy.includes(filter.taxonomy))){
        tmpRec.creatureList.push(creature);
      }
      return creature;
    });
    
    tmpList.push(tmpRec);
    return record;
  })
}