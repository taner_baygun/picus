import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import "./Css/Slider.css";
const useStyles = makeStyles({
  root: {
    width: 350,
  },
});


export default function DiscreteSlider(props) {
  const classes = useStyles();

  return (
    <div className={`${classes.root} slider-container`}>
      <div className="slider-text">
        Sample Rate
      </div>
      <div className="slide-container">
        <Slider
          defaultValue={5}
          aria-labelledby="discrete-slider"
          valueLabelDisplay="auto"
          step={1}
          marks
          min={5}
          max={20}
          onChange={props.onChange}
          style={{padding: "25px 0 15px 0"}}
        />
      </div>
      <div className="slider-value">
        {props.sampleRate}
      </div>
    </div>
  );
}
