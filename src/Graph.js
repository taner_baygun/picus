import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
  CartesianGrid,
  Brush
} from 'recharts';
import moment from 'moment';
import { deadOrAliveCount } from './Utility.js';
import './Css/Graph.css';



const CustomTooltip = ({ active, payload, label }) => {
  if (payload === null) return;
  
  if (active){
    return (
      <div className="custom-tooltip">
        <div className="value-tooltip">Alive: {payload[0].value}</div>
        <div className="value-tooltip">Dead: {payload[1].value}</div>
        <div className="value-tooltip">Unknown: {payload[2].value}</div>
      </div>
    );
  }
    
  return null;
};

const dateFormatter = (item, format) => moment(item).format(format);

function DrawBarChart(entry){
  let {data} = entry;
  let {numberOfItems} = entry;

  
  let processedData = data.map((item) => {
    let statusCount = deadOrAliveCount(item.creatureList);
    return {"date":  dateFormatter(item.date, "DD.MM.YY"), "shortDate": dateFormatter(item.date, "DD.MM"), "alive": statusCount[1].length, "dead": statusCount[0].length, "unknown": statusCount[2].length}
  })

  let fontSize = Math.max(Math.min(100/numberOfItems, 21), 15);
  return(
    <ResponsiveContainer width={800} height={400}>
      <BarChart data={processedData}>
        <XAxis  dataKey="date" fontSize={fontSize}/>
        <YAxis fontSize={21} />
        <CartesianGrid strokeDasharray="3 3" />
        <Legend />
        
        <Brush
          dataKey="shortDate"
          startIndex={0}
          endIndex={numberOfItems - 1}
        />
        
        <Tooltip content={<CustomTooltip />}/>
        
        <Bar dataKey="alive" fill="rgba(0, 120, 0, 0.75)" />
        <Bar dataKey="dead" fill="rgba(200, 0, 0, 0.75)" />
        <Bar dataKey="unknown" fill="rgba(40, 40, 40, 0.75)" />
      </BarChart>
    </ResponsiveContainer>
  )
}

export default DrawBarChart