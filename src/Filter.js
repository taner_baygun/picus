
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import './Css/Filter.css'

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));



export default function SimpleSelect(props) {
  const classes = useStyles();
  const [age_value, setAgeValue] = React.useState("")
  const [diet_value, setDietValue] = React.useState("")
  const [taxonomy_value, setTaxonomyValue] = React.useState("")


  const onChange = (category) => (e) => {
    let value = e.target.value;
    if (category === "reset") {
      setAgeValue("");
      setDietValue("");
      setTaxonomyValue("");
    } else {
      switch (category){
        case "age":
          setAgeValue(value);
          break;
        case "diet":
          setDietValue(value);
          break;
        default:
          setTaxonomyValue(value);
          break;
      }
      
    }
    
    props.onChange(category, value);
  }

  return (
    <div className="filterContainer">
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel>Age</InputLabel>
        <Select
          onChange={onChange("age")}
          value={age_value}
          label="Age"
        >
          <MenuItem value="default">All</MenuItem>
          <MenuItem value="young">Young</MenuItem>
          <MenuItem value="adult">Adult</MenuItem>
          <MenuItem value="elder">Elder</MenuItem>
        </Select>
      </FormControl>
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel>Diet</InputLabel>
        <Select
          onChange={onChange("diet")}
          value={diet_value}
          label="Diet"
        >
          <MenuItem value="default">All</MenuItem>
          <MenuItem value="carnivore">Carnivore</MenuItem>
          <MenuItem value="herbivore">Herbivore</MenuItem>
        </Select>
      </FormControl>
      <TextField
          id="outlined-required"
          label="Taxonomy"
          variant="outlined"
          value={taxonomy_value}
          className="filterTextField"
          onChange={onChange("taxonomy")}
      />
      <Button className="filterResetButton" variant="contained" color="primary" href="#contained-buttons" onClick={onChange("reset")}>
        Reset
      </Button>
    </div>
  );
}