import React from 'react';
import './Css/App.css';
import Slider from './Slider.js';
import Filter from './Filter.js';
import { recordList } from './Constants.js';
import { filterList, deadOrAliveCount } from './Utility.js';
import BarGraph from './Graph';

const configuration = {
  "age": "",
  "diet": "",
  "taxonomy": ""
};

function App() {
  
  const [sampleRate, setSampleRate] = React.useState(5)
  const [filteredList, applyFilter] = React.useState(recordList)
  
  const onSlideChange = (e, value) => {
    setSampleRate(value);
  }

  

  const onFilter = (category, value) => {
    
    if (category === "reset"){
      configuration.age = "";
      configuration.diet = "";
      configuration.taxonomy = "";
    } else if (value === "default"){
      configuration[`${category}`] = "";
    } else {
      configuration[`${category}`] = value;
    }
    let tmpList = [];
    
    filterList(tmpList, configuration);
    applyFilter(tmpList);
  }

  let lastRecord = filteredList[filteredList.length - 1];
  let data = deadOrAliveCount(lastRecord.creatureList);
  let alive = data[1].length;
  let dead = data[0].length;

  let percentage = ((alive / (dead + alive)) * 100).toFixed(2);
  
  return (
    <div className="App">
      <div className="App-body">
          <div className="survivalRate">{`Survival percentage: ${percentage}%`}</div>
          <Filter onChange={onFilter}/>
          <Slider sampleRate={sampleRate} onChange={onSlideChange}/>
          <BarGraph data={filteredList} numberOfItems={sampleRate}/>
      </div>
    </div>
  );
}



export default App;
